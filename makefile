# Makefile for Docker Compose actions

# Define the default Docker Compose command
DOCKER_COMPOSE := docker-compose

.PHONY: up down start stop restart logs full-restart

# Command to start the Docker Compose stack
up:
	$(DOCKER_COMPOSE) up -d

rm:
	$(DOCKER_COMPOSE) rm

build:
	$(DOCKER_COMPOSE) build

# Command to stop the Docker Compose stack
down:
	$(DOCKER_COMPOSE) down

# Command to start the Docker Compose stack
start:
	$(DOCKER_COMPOSE) start

# Command to stop the Docker Compose stack
stop:
	$(DOCKER_COMPOSE) stop

# Command to restart the Docker Compose stack
restart:
	$(DOCKER_COMPOSE) restart

# Command to view logs of the Docker Compose stack
logs:
	$(DOCKER_COMPOSE) logs -f

# Command to start the Docker Compose stack with additional flags
up-with-flags:
	$(DOCKER_COMPOSE) up -d --remove-orphans

# Command to stop and remove the Docker Compose stack with additional flags
down-with-flags:
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

# Command to restart the Docker Compose stack with additional flags
restart-with-flags:
	$(DOCKER_COMPOSE) restart --no-deps

# Command to perform a full restart (stop and start) of the Docker Compose stack
full-restart: down start

clean: down rm

deep-clean: down-with-flags rm

fresh: down build up-with-flags logs

