# Simple script to run against SQS
# takes explicit queue name and polls for messages on it
# executes process_message() function when triggered

import os
import sys
import json
import time
import boto3
from botocore.exceptions import ClientError

import logging


QUEUE_NAME = os.environ.get('QUEUE_NAME', 'model-eval')
LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')


os.environ.setdefault('AWS_DEFAULT_REGION', 'us-west-2')
AWS_DEFAULT_REGION = os.environ.get('AWS_DEFAULT_REGION', 'us-west-2')


def setup_logging(log_level=logging.DEBUG):
    logging.basicConfig(format='%(levelname)s:%(message)s', level=log_level)
    return logging.getLogger(__name__)


sqs = boto3.resource('sqs')
log = setup_logging(LOG_LEVEL)


def get_queue(name=QUEUE_NAME):
    """
    Gets an SQS queue by name.
    :param name: The name that was used to create the queue.
    :return: A Queue object.
    """
    try:
        queue = sqs.get_queue_by_name(QueueName=name)
        log.info("Got queue '%s' with URL=%s", name, queue.url)
    except ClientError as error:
        log.exception("Couldn't get queue named %s.", name)
        raise error
    else:
        return queue


def receive_messages(queue, max_number=1, wait_time=2):
    """
    Receive a batch of messages in a single request from an SQS queue.
    :param queue: The queue from which to receive messages.
    :param max_number: The maximum number of messages to receive. The actual number
                       of messages received might be less.
    :param wait_time: The maximum time to wait (in seconds) before returning. When
                      this number is greater than zero, long polling is used. This
                      can result in reduced costs and fewer false empty responses.
    :return: The list of Message objects received. These each contain the body
             of the message and metadata and custom attributes.
    """
    try:
        messages = queue.receive_messages(
            MessageAttributeNames=['All'],
            MaxNumberOfMessages=max_number,
            WaitTimeSeconds=wait_time
        )
        for msg in messages:
            log.info("Received message: %s: %s", msg.message_id, msg.body)
    except ClientError as error:
        log.exception("Couldn't receive messages from queue: %s", queue)
        raise error
    else:
        return messages


def delete_message(message):
    """
    Delete a message from a queue. Clients must delete messages after they
    are received and processed to remove them from the queue.
    :param message: The message to delete. The message's queue URL is contained in
                    the message's metadata.
    :return: None
    """
    try:
        message.delete()
        log.info("Deleted message: %s", message.message_id)
    except ClientError as error:
        log.exception("Couldn't delete message: %s", message.message_id)
        raise error


def delete_messages(queue, messages):
    """
    Delete a batch of messages from a queue in a single request.
    :param queue: The queue from which to delete the messages.
    :param messages: The list of messages to delete.
    :return: The response from SQS that contains the list of successful and failed
             message deletions.
    """
    try:
        entries = [{
            'Id': str(ind),
            'ReceiptHandle': msg.receipt_handle
        } for ind, msg in enumerate(messages)]
        response = queue.delete_messages(Entries=entries)
        if 'Successful' in response:
            for msg_meta in response['Successful']:
                log.debug("Deleted %s", messages[int(
                    msg_meta['Id'])].receipt_handle)
        if 'Failed' in response:
            for msg_meta in response['Failed']:
                log.warning(
                    "Could not delete %s",
                    messages[int(msg_meta['Id'])].receipt_handle
                )
    except ClientError:
        log.exception("Couldn't delete messages from queue %s", queue)
    else:
        return response


def process_message(data):
    log.info(f'Processing Message: {data}')


def main():
    message_queue = get_queue()
    batch_size = 1

    log.info(
        f"Receiving, handling, and deleting messages in batches of {batch_size}.")
    more_messages = True
    poll_loop_delay = 1  # seconds
    while more_messages:
        received_messages = receive_messages(message_queue, batch_size, 2)

        for message in received_messages:
            log.debug(f'Received Message: {message.message_id}')
            process_message(message.body)
        if received_messages:
            delete_messages(message_queue, received_messages)
        else:
            log.debug(
                f'Sleeping for {poll_loop_delay} second before checking for new messages')
            time.sleep(poll_loop_delay)
            # more_messages = False
    log.info('Done.')


if __name__ == '__main__':
    main()
