# Simple script to run against SQS
# takes explicit queue name and polls for messages on it
# executes process_message() function when triggered

import os
import json
import boto3
from botocore.exceptions import ClientError

import logging


# Simple to change
message_dict = {
    "model": {
        "name": "test-model.1.2.3.4.5",
        "loose_change": {}  # Always give a valid ability to extend within the design
    }
}


QUEUE_NAME = os.environ.get('QUEUE_NAME', 'model-eval')
LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')

AWS_DEFAULT_REGION = os.environ.get('AWS_DEFAULT_REGION', 'us-west-2')


def setup_logging(log_level=logging.DEBUG):
    logging.basicConfig(format='%(levelname)s:%(message)s', level=log_level)
    return logging.getLogger(__name__)


sqs = boto3.resource('sqs')
log = setup_logging(LOG_LEVEL)


def get_queue(name=QUEUE_NAME):
    """
    Gets an SQS queue by name.
    :param name: The name that was used to create the queue.
    :return: A Queue object.
    """
    try:
        queue = sqs.get_queue_by_name(QueueName=name)
        log.info("Got queue '%s' with URL=%s", name, queue.url)
    except ClientError as error:
        log.exception("Couldn't get queue named %s.", name)
        raise error
    else:
        return queue


# Send message to SQS queue
def send_message(queue, message_body, message_attributes=None):
    """
    Send a message to an Amazon SQS queue.

    :param queue: The queue that receives the message.
    :param message_body: The body text of the message.
    :param message_attributes: Custom attributes of the message. These are key-value
                               pairs that can be whatever you want.
    :return: The response from SQS that contains the assigned message ID.
    """
    if not message_attributes:
        message_attributes = {}

    try:
        response = queue.send_message(
            MessageBody=message_body,
            MessageAttributes=message_attributes
        )
    except ClientError as error:
        log.exception("Send message failed: %s", message_body)
        raise error
    else:
        return response


def main():
    message_queue = get_queue()
    log.info(f'Sending Message: {message_dict}')

    response = message_queue.send_message(
        QueueUrl=message_queue.url,
        DelaySeconds=10,
        MessageAttributes={},
        MessageBody=json.dumps(message_dict)
    )
    log.info(response)


if __name__ == '__main__':
    main()
