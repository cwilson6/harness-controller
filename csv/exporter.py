import os
import csv
import time
import pymysql
from prometheus_client import start_http_server, Gauge

PROMETHEUS_EXPORTER_PORT = int(
    os.environ.get('PROMETHEUS_EXPORTER_PORT', "8000"))

# Specify the directory to monitor
CSV_DIR = os.environ.get("CSV_DIR", os.path.dirname(os.path.abspath(__name__)))

# Create a Prometheus Gauge metric
csv_files_metric = Gauge('csv_files', 'Number of CSV files in the directory')

PROCESSED_FILES = set()

# Database connection details
DB_HOST = os.environ.get('DB_HOST', 'mariadb')
DB_USER = os.environ.get('DB_USER', 'root')
DB_PASSWORD = os.environ.get('DB_PASSWORD', 'admin')
DB_NAME = os.environ.get('DB_NAME', 'model_eval')

# Define the column headers for each type of CSV
CSV_TYPES = {
    "Type-A": {
        'columns': ['timestamp', 'feature1', 'feature2', 'result']
    },
    "Type-B": {
        'columns': ['timestamp', 'feature3', 'metric1', 'result']
    },
    "Type-C": {
        'columns': ['timestamp', 'feature2', 'metric2', 'metric3', 'result']
    },
    "Type-D": {
        'columns': ['timestamp', 'feature1', 'metric1', 'metric2', 'result']
    },
    "Type-E": {
        'columns': ['timestamp', 'feature3', 'metric3', 'result']
    }
}


def get_db_connection():
    max_retries = 25  # Maximum number of connection attempts
    retry_interval = 5  # Time interval between each retry (in seconds)
    retries = 0

    while retries < max_retries:
        try:
            connection = pymysql.connect(
                host=DB_HOST, user=DB_USER, password=DB_PASSWORD)
            return connection
        except pymysql.Error:
            print("Failed to connect to the database. Retrying in {} seconds...".format(
                retry_interval))
            time.sleep(retry_interval)
            retries += 1

    raise Exception(
        "Failed to connect to the database after {} retries.".format(max_retries))

# Function to create the model_eval database if it doesn't exist


def create_database():
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            cursor.execute(f"CREATE DATABASE IF NOT EXISTS {DB_NAME}")
    finally:
        connection.close()

# Function to create tables for each CSV type


def create_tables():
    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            for csv_type in CSV_TYPES:
                table_name = f"{DB_NAME}.`{csv_type}`"
                columns = []
                for column in CSV_TYPES[csv_type]['columns']:
                    if column == 'timestamp':
                        columns.append(f"`{column}` DATETIME")
                    else:
                        columns.append(f"`{column}` FLOAT")
                columns_str = ', '.join(columns)
                query = f"CREATE TABLE IF NOT EXISTS {table_name} ({columns_str})"
                cursor.execute(query)
    finally:
        connection.close()

# Function to insert data from CSV into the appropriate table


def insert_data(file_path):
    file_name = os.path.basename(file_path)
    csv_type = file_name.split('_')[0]
    table_name = f"{DB_NAME}.`{csv_type}`"
    print(f'TABLE_NAME: ------ `{table_name}`')

    connection = get_db_connection()
    try:
        with connection.cursor() as cursor:
            with open(file_path, 'r') as csv_file:
                reader = csv.reader(csv_file)
                columns = next(reader)
                for row in reader:
                    # values = [f"`{value}`" for value in row]
                    # values_str = ', '.join(values)
                    # query = f"INSERT INTO {table_name} VALUES ({values_str})"
                    values = [value if value ==
                              'NULL' else f"'{value}'" for value in row]
                    columns_str = ', '.join(
                        f"`{column}`" for column in columns)
                    values_str = ', '.join(values)
                    query = f"INSERT INTO {table_name} ({columns_str}) VALUES ({values_str})"
                    print(query)
                    print(columns_str)
                    print(values)
                    print(values_str)
                    cursor.execute(query)
            connection.commit()
    finally:
        connection.close()

# Function to delete processed CSV files


def delete_files(file_paths):
    for file_path in file_paths:
        os.remove(file_path)


# Main function to monitor the directory and process new CSV files


def monitor_directory():
    # --> Place the code to generate the CSVs here if desired
    while True:
        for file_name in os.listdir(CSV_DIR):
            file_path = os.path.join(CSV_DIR, file_name)
            if os.path.isfile(file_path) and file_path not in PROCESSED_FILES:
                insert_data(file_path)
                PROCESSED_FILES.add(file_path)
        delete_files(PROCESSED_FILES)


def main():
    print("Starting Prometheus Exporter HTTP Server on port 8000")
    start_http_server(PROMETHEUS_EXPORTER_PORT)
    create_database()
    create_tables()
    monitor_directory()


if __name__ == '__main__':
    main()
