import csv
import random
from datetime import datetime, timedelta

# Define the column headers for each type of CSV
csv_types = [
    {
        'name': 'Type A',
        'columns': ['timestamp', 'feature1', 'feature2', 'result']
    },
    {
        'name': 'Type B',
        'columns': ['timestamp', 'feature3', 'metric1', 'result']
    },
    {
        'name': 'Type C',
        'columns': ['timestamp', 'feature2', 'metric2', 'metric3', 'result']
    },
    {
        'name': 'Type D',
        'columns': ['timestamp', 'feature1', 'metric1', 'metric2', 'result']
    },
    {
        'name': 'Type E',
        'columns': ['timestamp', 'feature3', 'metric3', 'result']
    }
]

# Generate and save 20 CSV files
for i in range(20):
    csv_type = random.choice(csv_types)
    filename = f'csv/csv/{csv_type["name"].replace(" ", "_")}_{i+1}_.csv'

    # Generate a random number of rows between 10 and 100
    num_rows = random.randint(10, 100)

    with open(filename, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_type['columns'])
        writer.writeheader()

        # Generate random data for each row
        for _ in range(num_rows):
            timestamp = datetime.now() - timedelta(minutes=random.randint(1, 10000))
            result = random.choice([0, 1])

            row_data = {
                'timestamp': timestamp.strftime('%Y-%m-%d %H:%M:%S'),
                'result': result
            }

            # Generate random data for the remaining columns of the specific CSV type
            for column in csv_type['columns'][1:-1]:
                row_data[column] = random.uniform(0, 1)

            writer.writerow(row_data)

    print(f'Saved file: {filename}')
